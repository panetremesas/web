import React from 'react'
import ImageCarousel from '../components/ImageCarousel'

export const HomePage = () => {
    return (
        <div style={{ width: '100vw', height: '85vh', overflow: 'hidden' }}>
            <ImageCarousel />
        </div>
    )
}
