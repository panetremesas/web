import React from 'react';
import { MyRoutes } from './routers/routes';
import { Header } from './components/Header';
import { Analytics } from "@vercel/analytics/react"
import { SpeedInsights } from "@vercel/speed-insights/next"

function App() {
  return (
    <>
    <Analytics/>
    <SpeedInsights/>
      <Header />
      <div className="content">
        <MyRoutes />
      </div>
    </>
  );
}

export default App;
