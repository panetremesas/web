import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React from 'react'
import {HomePage} from "../pages/HomePage";
export const MyRoutes = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
      </Routes>
    </Router>
  )
}
