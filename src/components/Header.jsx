import React from 'react';
import backgroundImage from '../assets/header.png';
import leftImage from '../assets/logo.png'
import DropdownMenu from './DropdownMenu';

export const Header = () => {
    return (
        <div className="fixed top-0 left-0 w-full bg-cover" style={{ backgroundImage: `url(${backgroundImage})`, height: '15vh', zIndex: '10' }}>
            <div className="flex justify-between items-center h-full px-4">
                <div className="flex items-center">
                    <img src={leftImage} alt="Left Image" style={{ width: '20%' }} />
                </div>

                <div className="flex items-center mt-4 md:mt-0 md:ml-4" >
                    <input
                        type='text'
                        className='rounded-2xl pr-4 mr-2 bg-gray-100 text-lg p-1 text-right placeholder-[#053044] placeholer-text-bold text-bold'
                        placeholder='TRACKING'
                        style={{ width: '25vw' }}
                    />
                    <DropdownMenu />
                </div>
            </div>
        </div>
    );
}
