import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import image1 from '../assets/carousel/01.png';
import image2 from '../assets/carousel/02.png';
import image3 from '../assets/carousel/03.png';
import image4 from '../assets/carousel/04.png';
import image5 from '../assets/carousel/05.png';

const ImageCarousel = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
    };

    return (
        <Slider {...settings}>
            <div>
                <img src={image1} style={{ width: '100%', height: '85vh', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image2} style={{ width: '100%', height: '85vh', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image3} style={{ width: '100%', height: '85vh', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image4} style={{ width: '100%', height: '85vh', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image5} style={{ width: '100%', height: '85vh', objectFit: 'cover' }} />
            </div>
        </Slider>
    );
};

export default ImageCarousel;
